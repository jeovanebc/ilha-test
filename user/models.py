from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.hashers import make_password, check_password
from django.db import models

# Create your models here.
from django.http import HttpResponse


class UserAccountManager(BaseUserManager):

    def create_user(self, name, email, password):
        user = User(username=name, email=email, password=password)
        user.save()
        return HttpResponse(user)

    def create_superuser(self, name, email, password):
        user = self.create_user(name, email, password)
        user.is_admin = True
        user.save(using=self._db)
        return HttpResponse(user)

    def get_by_natural_key(self, name):
        return self.get(username=name)


class User(models.Model):
    username = models.CharField(max_length=200, null=False, blank=False, unique=True)
    email = models.EmailField(max_length=50, null=False, blank=False)
    password = models.CharField(max_length=50, null=False, blank=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    is_anonymous = False
    is_authenticated = True
    check_password = True
    is_active = True
    objects = UserAccountManager()

    def set_password(self, raw_password):
        self.password = raw_password

    def has_complete_information(self):
        return (self.username is not None) and (self.email is not None) and (self.password is not None)

    def has_valid_password(self):
        return len(self.password) >= 6

    def encrypt_password(self):
        self.password = make_password(self.password, salt=None, hasher='default')

    def user_can_authenticate(self):
        return True

    def check_password(self, raw_password):
        def setter(raw_password):
            self.set_password(raw_password)
            # Password hash upgrades shouldn't be considered password changes.
            self._password = None
            self.save(update_fields=["password"])
        return check_password(raw_password, self.password, setter)