# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets
from rest_framework.decorators import renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.utils import json

from ilhasoft import serializers
from user.models import User


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializer

    def register_form(self):
        return render(self, 'user/register.html')

@csrf_exempt
@renderer_classes([JSONRenderer])
def create_user(request):
    body = request.POST
    username = body.get('username')
    email = body.get('email')
    password = body.get('password')
    user = User(username=username, email=email, password=password)
    if user.has_complete_information() is True:
        if user.has_valid_password():
            user.encrypt_password()
            user = user.save(user)
            return HttpResponse("Usuário criado com sucesso")
        else:
            raise Exception("Senha inválida")
    else:
        raise Exception("Dados incompletos")