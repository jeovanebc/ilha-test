from django.contrib.postgres.fields import ArrayField
from django.db import models

# Create your models here.


class TokenInformation(models.Model):
    client_id = models.CharField(max_length=500, default="31fbac928356411a8db2d997adc1f569")
    secret = models.CharField(max_length=500, default="f18d087527e5437fb98fdab07d4536d3")
    token_type = models.CharField(max_length=500)
    expires_in = models.CharField(max_length=500)
    refresh_token = models.CharField(max_length=500)
    scope = models.CharField(max_length=500)
    access_token = models.CharField(max_length=500)


class Playlist(models.Model):
    name = models.CharField(max_length=200, null=False, blank=False)
    spotify_id = models.CharField(max_length=50, null=False, blank=False)
    collaborative = models.BooleanField()


class MyTrack(models.Model):
    album_name = models.CharField(max_length=200, null=False, blank=False, default="")
    total_total_track = models.BigIntegerField(default=0)
    artist_name = models.CharField(max_length=500, null=False, blank=False, default="")
    artist_id = models.CharField(max_length=50, null=False, blank=False, default="")
    track_id = models.CharField(max_length=50, null=False, blank=False, default="")


