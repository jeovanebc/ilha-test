# Create your views here.
# from requests import Response
import webbrowser

import requests
import spotipy
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.utils import json
from spotipy import oauth2

from ilhasoft import serializers
from spotify.models import TokenInformation, Playlist, MyTrack


class SpotifyViewSet(viewsets.ModelViewSet):
    queryset = MyTrack.objects.all()
    serializers_class = serializers.MyTrack

    def login_spotify(self):
        sp_oauth = oauth2.SpotifyOAuth(scope='user-read-email, '
                                             'user-library-read, playlist-modify-public, playlist-read-collaborative,'
                                             ' playlist-modify-private, playlist-modify-public, playlist-read-private',
                                       client_id='31fbac928356411a8db2d997adc1f569',
                                       client_secret='f18d087527e5437fb98fdab07d4536d3',
                                       # show_dialog="true",
                                       redirect_uri='http://39a13f42.ngrok.io/login-complete/')

        auth_url = sp_oauth.get_authorize_url()
        webbrowser.open(auth_url)
        return HttpResponse()

    def login_complete(self):
        code = self.GET.get("code")
        base_64_token = "MzFmYmFjOTI4MzU2NDExYThkYjJkOTk3YWRjMWY1Njk6ZjE4ZDA4NzUyN2U1NDM3ZmI5OGZkYWIwN2Q0NTM2ZDM="
        result = requests.post("https://accounts.spotify.com/api/token",
                               headers={"Authorization": "Basic {}".format(base_64_token)},
                               data={
                                   "grant_type": "authorization_code",
                                   "code": code,
                                   "redirect_uri": "http://39a13f42.ngrok.io/login-complete/"
                               }
                               )
        auth_information_as_json = result.json()
        tt = auth_information_as_json.get("token_type")
        ex = auth_information_as_json.get("expires_in")
        rt = auth_information_as_json.get("refresh_token")
        s = auth_information_as_json.get("scope")
        at = auth_information_as_json.get("access_token")
        # if TokenInformation.objects.all().count() <= 0:
        spotify_to_save = TokenInformation(token_type=tt, expires_in=ex, refresh_token=rt, scope=s, access_token=at)
        spotify_to_save.save()
        if Playlist.objects.all().count() == 0:
            new_playlist = Playlist(name="Ilha-test", spotify_id="2PowEiF7j8VR3dLlMTED6c", collaborative=True)
            new_playlist.save()
        return HttpResponse()

    def get_playlist(self):
        playlist = MyTrack.objects.all()
        return HttpResponse(playlist)

    def search_music(self):
        global founded_tracks
        track = self.GET.get("text")
        token_information = TokenInformation.objects.last()
        spotipy_client = spotipy.client.Spotify(auth=token_information.access_token)
        tracks = spotipy_client.search(track)
        if tracks:
            founded_tracks = []
            items = tracks.get("tracks").get("items")
            for item in items:
                track_id = item.get("id")
                album = item.get("album")
                album_name = album.get("name")
                album_total_tracks = album.get("total_tracks")
                album_artists = album.get("artists")
                artist = album_artists[0]
                artist_name = artist.get("name")
                artist_id = artist.get("id")
                data = {
                    "Album Name": album_name,
                    "Total Album Track": album_total_tracks,
                    "Artist Name": artist_name,
                    'Artist Id': artist_id,
                    "Track Id": track_id
                }
                founded_tracks.append(data)
        return HttpResponse(json.dumps(founded_tracks))

    @csrf_exempt
    def add_music_to_playlist(self):
        body_unicode = self.body.decode('utf-8')
        body_data = json.loads(body_unicode)
        token_information = TokenInformation.objects.last()
        spotipy_client = spotipy.client.Spotify(auth=token_information.access_token)

        for result in body_data:
            track = spotipy_client.track(result)
            track_id = track.get("id")
            album = track.get("album")
            album_name = album.get("name")
            album_total_tracks = album.get("total_tracks")
            album_artists = album.get("artists")
            artist = album_artists[0]
            artist_name = artist.get("name")
            artist_id = artist.get("id")
            my_track = MyTrack(track_id=track_id,
                               album_name=album_name,
                               total_total_track=album_total_tracks,
                               artist_id=artist_id,
                               artist_name=artist_name)
            my_track.save()
            result = spotipy_client.user_playlist_add_tracks("31fbac928356411a8db2d997adc1f569", "2PowEiF7j8VR3dLlMTED6c", [track_id])
            print(result)
        playlist = MyTrack.objects.all()
        return HttpResponse(playlist)