from rest_framework import serializers

from spotify.models import TokenInformation, MyTrack
from user.models import User


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email', 'password']


class SpotifySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TokenInformation
        fields = ["client_id", "secret", "token_type", "expires_in", "refresh_token", "scope", "access_token"]


class PlaylistSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TokenInformation
        fields = ['name', 'spotify_id', 'tracks', 'collaborative']


class MyTrackSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = MyTrack
        fields = [
            "album_name",
            "total_total_track",
            "artist_name",
            "artist_id"
        ]

